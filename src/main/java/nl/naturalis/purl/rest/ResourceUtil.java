package nl.naturalis.purl.rest;

import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Variant;

import nl.naturalis.nba.common.json.JsonUtil;
import nl.naturalis.nba.utils.http.SimpleHttpGet;

import static nl.naturalis.purl.Messages.INTERNAL_SERVER_ERROR;
import static nl.naturalis.purl.Messages.NOT_ACCEPTABLE;
import static nl.naturalis.purl.Messages.NOT_FOUND;
import static nl.naturalis.purl.Messages.SEE_OTHER;

/**
 * Utility class providing REST-related functionality.
 * 
 * @author Ayco Holleman
 */
@SuppressWarnings("unused")
public class ResourceUtil {

  private ResourceUtil() {}

  /**
   * Equivalent to {@code URLEncoder.encode(s, "UTF-8")}.
   * 
   * @param raw The {@code String} to encode
   * @return url encoded string
   */
  public static String urlEncode(String raw) {
    return URLEncoder.encode(raw, StandardCharsets.UTF_8);
  }

  /**
   * Performs a 303 (See Other) redirect to the specified location
   * 
   * @param location URI
   * @return response
   */
  public static Response redirect(URI location) {
    return Response.seeOther(location).build();
  }

  /**
   * Retrieves and serves up the content at the specified location.
   * 
   * @param location URI
   * @param mediaType MediaType
   * @return response
   */
  public static Response load(URI location, MediaType mediaType) {
    SimpleHttpGet request = new SimpleHttpGet();
    request.setBaseUrl(location);
    byte[] source = request.execute().getResponseBody();
    return Response.ok().entity(source).type(mediaType).build();
  }

  /**
   * Shows the location to which the PURL server would redirect, but does not actually redirect.
   * 
   * @param location URI
   * @return response
   */
  public static Response redirectDebug(URI location) {
    String message = SEE_OTHER + location;
    return plainTextResponse(message);
  }

  /**
   * Generate an HTTP response with status 500 (INTERNAL SERVER ERROR) and the specified message in the response body. The content type of
   * the response body is set to text/plain.
   * 
   * @param message string
   * @return response 500
   */
  public static Response serverError(String message) {
    message = INTERNAL_SERVER_ERROR + message;
    return ResourceUtil.plainTextResponse(500, message);
  }

  /**
   * Generate a 404 (NOT FOUND) response with the specified message in the response body.
   * 
   * @param objectType objectType
   * @param objectID objectID
   * @return Response 404
   */
  public static Response notFound(String objectType, String objectID) {
    String message = NOT_FOUND + String.format("No %s exists with ID \"%s\"", objectType, objectID);
    return plainTextResponse(404, message);
  }

  /**
   * Generate a 406 (NOT ACCEPTABLE) response with the specified list of acceptable alternative media types both in the response header and
   * the response body.
   * 
   * @param variants List of variants
   * @return response
   */
  public static Response notAcceptable(List<Variant> variants) {
    StringBuilder sb = new StringBuilder(200);
    sb.append(NOT_ACCEPTABLE);
    sb.append("Acceptable media types for this object: ");
    if (variants == null || variants.size() == 0) {
      sb.append(" none!");
    } else {
      sb.append(getVariantsAsString(variants));
    }
    return Response.notAcceptable(variants)
        .entity(sb.toString())
        .type(MediaType.TEXT_PLAIN)
        .build();
  }

  /**
   * Generate a 200 (OK) response with the specified message in the response body and a Content-Type header of text/plain.
   * 
   * @param message string
   * @return response 200
   */
  public static Response plainTextResponse(String message) {
    return Response.ok(message, MediaType.TEXT_PLAIN).build();
  }

  /**
   * Generate a response with the specified HTTP status code, the specified message in the response body and a Content-Type header of
   * text/plain.
   * 
   * @param status int
   * @param message string
   * @return response
   */
  public static Response plainTextResponse(int status, String message) {
    return Response.status(status).entity(message).type(MediaType.TEXT_PLAIN).build();
  }

  /**
   * Give a print-out of the specified object.
   * 
   * @param bean your java bean
   * @return String representation of your java bean
   */
  public static String dump(Object bean) {
    return JsonUtil.toJson(bean);
  }

  private static String getVariantsAsString(List<Variant> variants) {
    return variants.stream().map(v -> v.getMediaType().toString()).collect(Collectors.joining(","));
  }

}
